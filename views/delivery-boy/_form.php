<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="delivery-boy-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="col-md-5">
    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Nombre del Repartidor'])->label(false) ?>
</div>
<div class="col-md-5">
<?= $form->field($model, 'phone')->textInput(['maxlength' => true, 'placeholder' => 'Teléfono del Repartidor'])->label(false) ?>
</div>
<div class="col-md-1">

        <?= Html::submitButton($model->isNewRecord ? 'Agregar Repartidor' : 'Actualizar Repartidor', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
