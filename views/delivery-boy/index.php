<?php

use yii\helpers\Html;
use kartik\grid\GridView;
// use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use yii\helpers\Json;
use yii\db\Expression;
use yii\helpers\Url;
use app\models\Order;

?>
<div class="delivery-boy-index">

    <p>
        <?= Html::a('Agregar Repartidor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>


<?php $gridColumns =  [
    [
      'class' => 'kartik\grid\SerialColumn',
      'contentOptions'=>['class'=>'kartik-sheet-style'],
      'width'=>'36px',
      'header'=>'',
      'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
    [
      'class'=>'kartik\grid\ExpandRowColumn',
      'width'=>'50px',
      'value'=>function ($model, $key, $index, $column) {
          return GridView::ROW_COLLAPSED;
      },
      'detail'=>function ($model, $key, $index, $column) {
          return Yii::$app->controller->renderPartial('_form', ['model'=>$model,
        'rowvalue'=>$column->grid->dataProvider->totalCount - $index + 0]);
      },
      'headerOptions'=>['class'=>'kartik-sheet-style'],
      'expandOneOnly'=>true
    ],
    [
      'attribute'=>'name',
      'vAlign'=>'middle',
      'hAlign'=>'right',
        'width'=>'180px',
      // 'format'=>'time',
      'pageSummary'=>false
    ],


    [
      'class'=>'kartik\grid\CheckboxColumn',
    ],
    [
              'class' => 'kartik\grid\ActionColumn',
              'header'=>false,
              // 'pageSummary' => false,
              'options'=>['style'=>'width:150px;'],
              'buttonOptions'=>['class'=>'btn btn-default'],
              'template'=>'<div class="btn-group btn-group-sm text-center" role="group">
              {view}
              </div>',
              'buttons'=>[
                // 'print'=>function($url,$model){
                //   return Html::a('<i class="glyphicon glyphicon-print"></i>',['order/view'],['class'=>'btn-pdfprint btn btn-default','data-pjax'=>'0']);
                // }
              ]
            ],
]?>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns'=>$gridColumns,
            'resizableColumns'=>true,
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
        'toolbar'=> [
            '{export}',
            '{toggleData}',
        ],
        'bordered'=>true,
        'striped'=>true,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_WARNING,
            'heading'=>'Repartidores',
        ],
        'persistResize'=>true,
        'toggleDataOptions'=>['minCount'=>10],
        'export'=>[
              'fontAwesome'=>true,
              'PDF' => [
                      'options' => [
                      ]
                  ],
              ],
        'exportConfig' => [
              GridView::EXCEL => [
                'label' => 'Guardar en XLS',
                'showHeader' => true,
              ],
              GridView::PDF => [
                'label' => 'Guardar en PDF',
                'showHeader' => true,
                'showCaption' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                 'title' => 'Reporte Piccolinas Pizza',
                'options' => ['title' =>'Reporte Piccolinas Pizza'],
                'config' => ['options' => ['title' =>'Reporte Piccolinas Pizza'],],
                'filename' =>'Reporte Piccolinas Pizza',
              ],
    ]]); ?>
<?php Pjax::end(); ?></div>
