<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

// use app\models\Status;
use app\models\Order;
use app\models\OrderSearch;
use kartik\select2\Select2;
use kartik\editable\Editable;

?>
<?php $tituloexport = 'Ordenes del día '.$now?>
<?php $searchFilter= new OrderSearch();?>
<?php $this->title = $tituloexport?>
<div class="order-indextoday">
  <?php $rowvalue =   function ($model, $key, $index, $column) {
        return $column->grid->dataProvider->totalCount - $index + 0;}?>



<?php $gridColumns =  [
    [
      'attribute' => 'count',
      // 'value' => $model->count,
      'contentOptions'=>['class'=>'kartik-sheet-style'],
      'width'=>'36px',
      'header'=>'',
      'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
    [
      'class'=>'kartik\grid\ExpandRowColumn',
      'width'=>'50px',
      'value'=>function ($model, $key, $index, $column) {
          return GridView::ROW_COLLAPSED;
      },
      'detail'=>function ($model, $key, $index, $column) {
          return Yii::$app->controller->renderPartial('_detail_indextoday', [
            'model'=>$model,
            'rowvalue'=>$column->grid->dataProvider->totalCount - $index + 0
          ]);
      },
      'headerOptions'=>['class'=>'kartik-sheet-style'],
      'expandOneOnly'=>true
    ],

    [
    'attribute'=>'phone',
    'width'=>'310px',
    'pageSummary'=>false,

    'value'=>function ($model, $key, $index, $widget) {
        return $model->phone;
    },
    'filterType'=>GridView::FILTER_SELECT2,
    'filter'=>ArrayHelper::map(Order::find()->orderBy('phone')->asArray()->all(), 'id', 'phone'),
    'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
    ],
    'filterInputOptions'=>['placeholder'=>''],
    'group'=>false,  // enable grouping
    'groupedRow'=>false,
    'groupOddCssClass'=>'kv-grouped-row',
    'groupEvenCssClass'=>'kv-grouped-row',

],

    [
      'attribute'=>'time',
      'vAlign'=>'middle',
      'hAlign'=>'right',
      'width'=>'9%',
      'format'=>'time',
      'pageSummary'=>false
    ],

    [
      'attribute'=>'total',
      'vAlign'=>'middle',
      'hAlign'=>'right',
      'width'=>'7%',
      'format'=>['decimal', 2],
      'pageSummary'=>true
    ],
    [
              'class' => 'kartik\grid\ActionColumn',
              'header'=>false,
              // 'pageSummary' => false,
              'options'=>['style'=>'width:150px;'],
              'buttonOptions'=>['class'=>'btn btn-default'],
              'template'=>'<div class="btn-group btn-group-sm text-center" role="group">

              {view}

              </div>',
              'buttons'=>[

                // 'print'=>function($url,$model){
                //   return Html::a('<i class="glyphicon glyphicon-print"></i>',['order/view'],['class'=>'btn-pdfprint btn btn-default','data-pjax'=>'0']);
                // }
              ]
            ],

]?>

<?= GridView::widget([
    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'columns'=>$gridColumns,
    // 'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'pjax'=>true, // pjax is set to always true for this demo
    // set your toolbar
    'toolbar'=> [
        '{export}',
        '{toggleData}',
    ],

    // parameters from the demo form
    'bordered'=>true,
    'striped'=>true,
    'condensed'=>true,
    'responsive'=>true,
    'hover'=>true,
    'showPageSummary'=>false,
    'panel'=>[
        'type'=>GridView::TYPE_PRIMARY,
        'heading'=>'Ordenes de Hoy',
    ],
    'persistResize'=>true,
    'toggleDataOptions'=>['minCount'=>10],
    'export'=>[
          // 'fontAwesome' => true,
          'fontAwesome'=>true,
          'PDF' => [
                  'options' => [
                      // 'title' => $tituloexport,
                      //  'subject' => $tituloexport,
                      // 'author' => 'NYCSPrep CIMS',
                      // 'keywords' => 'NYCSPrep, preceptors, pdf'
                  ]
              ],
          ],
    'exportConfig' => [
          GridView::EXCEL => [
            'label' => 'Guardar en XLS',
            'showHeader' => true,
            // 'filename' => $tituloexport,


          ],
          GridView::PDF => [
            'label' => 'Guardar en PDF',
            'showHeader' => true,
            'showCaption' => true,
            'showPageSummary' => true,
            'showFooter' => true,
            'title' => $tituloexport,
            'options' => ['title' => $tituloexport, 'author' => 'Piccolinas Pizza'],
            'config' => ['options' => ['title' => $tituloexport],],
            'filename' => $tituloexport,
          ],
    ]
]);?>


</div>
