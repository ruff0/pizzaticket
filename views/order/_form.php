<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;
use kartik\select2\Select2;
use kartik\time\TimePicker;
use modernkernel\jnumber\JNumberInput;
use yii\helpers\Json;
use yii\db\Expression;
use kartik\date\DatePicker;
use app\models\Order;
use app\models\OrderSearch;

?>

<div class="order-form">
  <?php $request = Yii::$app->request;
  $model->phone = $request->get('phone');
  $model->address = $request->get('address');
  $model->neighborhood = $request->get('neighborhood');
  $model->delivery = $request->get('delivery');
  $model->size = $request->get('size');
  $model->ingredients = $request->get('ingredients');
  $model->quantity = $request->get('quantity');
  $model->cheese_border = $request->get('cheese_border');
  $model->money = $request->get('money');
  $model->change = $request->get('change');
  $model->subtotal = $request->get('subtotal');
  $model->total = $request->get('total');
  $model->wait = $request->get('wait');
  $model->count = $request->get('count');
  ?>
  <?php $this->registerJs('$("form input:checkbox, form checkbox").first().focus();');?>
  <?php
          $expression = new Expression('NOW()');
            $now = (new \yii\db\Query)->select($expression)->scalar();
            $now = substr($now, 0, 10);
            $itemsnow =  OrderSearch::find()->andFilterWhere(['like', 'date', $now])->count();
            $maxnumbercount = OrderSearch::find()->andFilterWhere(['like', 'date', $now])->orderBy(['id' => SORT_DESC])->one();
            if (($itemsnow == 0)||($itemsnow == null)||($itemsnow == false)){
              $numbercounttoday = 1;
            }
            else { $numbercounttoday = $maxnumbercount->count;
              $numbercounttoday = $numbercounttoday + 1;
            }
            $count = $itemsnow + 1;?>
    <?php $form = ActiveForm::begin(['id' => 'order-form']); ?>
    <div class="form-group">
      <div >
        <?= $form->field($model, 'count')->textinput()->hiddenInput(['value' =>$numbercounttoday])->label(false)?>
      </div>
    <table>
      <tr>
          <td class="col-md-1">
  <?= $form->field($model, 'cheese_border')->textInput()->textArea()->label(false)->widget(SwitchInput::classname(), [
                   'value' => -1,
                   'options' => [ 'id' => 'cheese_border'],
                   'pluginOptions' => [
                      'handleWidth'=>120,
                      // 'animate' => true,
                      'onText'=>'Orilla con queso',
                      'offText'=>'Orilla sin queso',
                      'size' => 'medium',
                    ],
                  ]); ?>
          </td>
          <td class="col-md-2">
            <?= $form->field($model, 'size')->textInput(['style' => 'text-transform: uppercase'])->label('Tamaño');?>

          </td>
          <td class="col-md-9">
            <?= $form->field($model, 'ingredients')->textInput(['style' => 'text-transform: uppercase'])->label('Ingredientes');?>

          </td>
      </tr>
    </table>
    <table>
      <tr>

          <td class="col-md-3">
            <?= $form->field($model, 'address')->textInput(['maxlength' => true, 'placeholder'=>'', 'style' => 'text-transform: uppercase']) ?>
          </td>
          <td class="col-md-3">
            <?= $form->field($model, 'neighborhood')->textInput(['maxlength' => true, 'placeholder'=>'', 'style' => 'text-transform: uppercase']) ?>
          </td>

          <td class="col-md-2">
            <?= $form->field($model, 'phone')->textInput(['maxlength' => 10, 'placeholder'=>'']) ?>
          </td>
      </tr>
    </table>

    <table>
      <tr class="col-md-13">

        <td class="col-md-1">
          <?= $form->field($model, 'quantity' )->textInput(['type' => 'number',
          'value'=>'1',
          'min' => 1,
          'id' => "quantity"]) ?>
        </td>
        <td class="col-md-1">
          <?= $form->field($model, 'subtotal')->textInput(['placeholder'=>'',
          'id' => "cost"])->label('Costo') ?>
        </td>
        <td class="col-md-1">
          <?= $form->field($model, 'delivery')->textInput(['placeholder'=>'',
          'id' => "delivery"]) ?>
        </td>

        <td class="col-md-1">
            <?= $form->field($model, 'total')->textInput(['placeholder'=>'', 'id' => "total"]) ?>
        </td>

        <td class="col-md-1">
          <?= $form->field($model, 'money')->textInput(['placeholder'=>'', 'id' => "pay"])->label('Pago') ?>
        </td>
        <td class="col-md-1">
        <?= $form->field($model, 'change')->textInput(['placeholder'=>'', 'id' => "change", 'disabled' => false, 'readOnly' => true]) ?>
        <!-- <?= $form->field($model, 'change')->widget(JNumberInput::className(), ['scale'=>2]) ?> -->
        </td>
        <td class="col-md-1">

              <?= $form->field($model, 'note')->label('Hora')->widget(TimePicker::classname(), [
                // 'value' => time(),

                // 'type' => 4,
                'readonly' => false,
                'disabled' => true,
                'size' => 'md',
                'pluginOptions' => [
                  'defaultTime' => 'current',
                  // 'format' => 'H:mm',
                  'minuteStep' => 1,
                  'showMeridian' => false,
                ],

              ]);?>
                <div style="display:none">
                              <?= $form->field($model, 'time')->widget(TimePicker::classname(), [

                                'readonly' => false,
                                'disabled' => false,
                                'pluginOptions' => [
                                  'defaultTime' => 'current',
                                  // 'format' => 'H:mm',
                                  'minuteStep' => 1,
                                  'showMeridian' => false,
                                ],

                              ]);?>
                </div>
        </td>
        <td class="col-md-1">
            <?= $form->field($model, 'wait')->textInput(['type' => 'number']);?>
        </td>
      </tr>
    </table>
    <?php
    $expression = new Expression('NOW()');
    $now = (new \yii\db\Query)->select($expression)->scalar();
    $now = substr($now, 0, 10);
    ?>
    <?= $form->field($model, 'only_date')->textInput()->hiddenInput(['value' => $now])->label(false); ?>

    <!-- <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?> -->

    <?= $form->field($model, 'status')->textInput()->hiddenInput(['value'=> 'Esperando'])->label(false); ?>
<table class="col-md-12">
  <tr>
    <td class="col-md-5 pull-right">

      <?= Html::submitButton($model->isNewRecord ? '<b>Ordenar</b>' : 'Guardar Modificación', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary', 'style' => 'width: 320px; border-radius: 10px;']) ?>
    </td>
    &nbsp;
    <!-- <td class="col-md-1"> -->
    <!-- <?= Html::submitButton('Borrar', ['class' => 'btn btn-danger']) ?> -->
    <!-- </td>  -->

  </tr>
</table>
</div>
    <?php ActiveForm::end(); ?>
</div>

<?php
$script = <<<EOD
$(function() {
     $('#quantity').click(function() {
        updateTotal();
        updateChange();
      });
    $('#cost').keyup(function() {
       updateTotal();
       updateChange();
     });
    $('#delivery').keyup(function() {
        updateTotal();
        updateChange();
      });
    $('#pay').keyup(function() {
        updateChange();
      });
    $('#total').keyup(function() {
        updateChange();
      });

    var updateTotal = function() {
      var cost = parseInt($('#cost').val());
      var delivery = parseInt($('#delivery').val());
      var quantity = parseInt($('#quantity').val());
      var subtotal = cost + delivery;

      if (isNaN(subtotal)){
        if (isNaN(cost))
        {
          $('#total').val(delivery);
        }
        else{
          $('#total').val(cost * quantity);
        }
      }
      else{
        $('#total').val((cost * quantity) + delivery);
      }
    };

    var updateChange = function() {
      var total = parseInt($('#total').val());
      var pay = parseInt($('#pay').val());
      var change = pay - total;
      var nothing = 0;

      if ((change < 0) || (isNaN(change))){
        $('#change').val('?')
      }
      else{
        $('#change').val(change);

      }
    }
 });
EOD;
$this->registerJs($script);
?>
