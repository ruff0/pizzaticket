<?php
use yii\helpers\Html;
// use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;
use app\models\Status;
use kartik\select2\Select2;
use yii\helpers\Json;
use yii\db\Expression;
use app\models\Order;
use app\models\OrderSearch;
use mickgeek\actionbar\Widget as ActionBar;
use yii\helpers\Url;
use kartik\daterange\DateRangePicker;

?>
<div class="order-index">

<?php $gridColumns =  [
    [
      'class' => 'kartik\grid\SerialColumn',
      'contentOptions'=>['class'=>'kartik-sheet-style'],
      'width'=>'36px',
      'header'=>'',
      'headerOptions'=>['class'=>'kartik-sheet-style']
    ],
    [
      'class'=>'kartik\grid\ExpandRowColumn',
      'width'=>'50px',
      'value'=>function ($model, $key, $index, $column) {
          return GridView::ROW_COLLAPSED;
      },
      'detail'=>function ($model, $key, $index, $column) {
          return Yii::$app->controller->renderPartial('_detail', ['model'=>$model,
        'rowvalue'=>$column->grid->dataProvider->totalCount - $index + 0]);
      },
      'headerOptions'=>['class'=>'kartik-sheet-style'],
      'expandOneOnly'=>true
    ],
    [
      'attribute'=>'count',
      'vAlign'=>'middle',
      'hAlign'=>'right',
        'width'=>'180px',
      // 'format'=>'time',
      'pageSummary'=>false
    ],

    [
    'attribute'=>'phone',
    'width'=>'310px',
    'pageSummary'=>false,

    'value'=>function ($model, $key, $index, $widget) {
        return $model->phone;
    },
    'filterType'=>GridView::FILTER_SELECT2,
    'filter'=>ArrayHelper::map(Order::find()->orderBy('phone')->asArray()->all(), 'id', 'phone'),
    'filterWidgetOptions'=>[
        'pluginOptions'=>['allowClear'=>true],
    ],
    'filterInputOptions'=>['placeholder'=>''],
    'group'=>false,  // enable grouping
    'groupedRow'=>false,
    'groupOddCssClass'=>'kv-grouped-row',
    'groupEvenCssClass'=>'kv-grouped-row',
],
    // [
    //   'attribute'=>'status',
    //   'vAlign'=>'middle',
    //   'width'=>'180px',
    //
    //   'filterType'=>GridView::FILTER_SELECT2,
    //   'filter'=>ArrayHelper::map(Status::find()->asArray()->all(), 'name', 'name'),
    //   'filterWidgetOptions'=>[
    //       'pluginOptions'=>['allowClear'=>true],
    //   ],
    //   'filterInputOptions'=>['placeholder'=>'Cualquier Status'],
    //   'format'=>'raw'
    // ],
    [
      'attribute'=>'only_date',
      // 'format' => 'YYYY-MM-DD',
      'options' => [
        'format' => 'YYYY-MM-DD',
      ],
      'vAlign'=>'middle',

      'hAlign'=>'right',
        'width'=>'180px',

      'pageSummary'=>false,


      'filterType' => GridView::FILTER_DATE_RANGE,
      'filterWidgetOptions' => ([
        'autoUpdateOnInit' => false,
        // 'model' => $model,
        'attribute' => 'only_date',
        'presetDropdown' => true,

        'convertFormat' => false,
        'pluginOptions' => [
          'useWithAddon' => true,
          'format' => 'YYYY-MM-DD',
          'opens' => 'left',
          'locale' => [
                'format' => 'YYYY-MM-DD'
            ],

          'separator' => ' - '
        ],
        'pluginEvents' => [
          // "apply.daterangepicker" => "function() { apply_filter('only_date') }",
        ],
      ])
    ],

    // [
    //   'attribute'=>'time',
    //   'vAlign'=>'middle',
    //   'hAlign'=>'right',
    //   'width'=>'9%',
    //   'format'=>'time',
    //   'pageSummary'=>false
    // ],
    [
      'attribute'=>'size',
      'vAlign'=>'middle',
      'hAlign'=>'right',
      'width'=>'9%',
      'format'=>'text',
      'pageSummary'=>false
    ],
    // [
    //   'attribute'=>'wait',
    //   'vAlign'=>'middle',
    //   'hAlign'=>'right',
    //   'width'=>'180px',
    //   // 'format'=>'time',
    //   'pageSummary'=>false
    // ],
    [
      'attribute'=>'total',
      'vAlign'=>'middle',
      'hAlign'=>'right',
      'width'=>'180px',
      'format'=>['decimal', 2],
      'pageSummary'=>true
    ],
    [
      'class'=>'kartik\grid\CheckboxColumn',
    ],
    [
              'class' => 'kartik\grid\ActionColumn',
              'header'=>false,
              // 'pageSummary' => false,
              'options'=>['style'=>'width:150px;'],
              'buttonOptions'=>['class'=>'btn btn-default'],
              'template'=>'<div class="btn-group btn-group-sm text-center" role="group">

              {view}

              </div>',
              'buttons'=>[

                // 'print'=>function($url,$model){
                //   return Html::a('<i class="glyphicon glyphicon-print"></i>',['order/view'],['class'=>'btn-pdfprint btn btn-default','data-pjax'=>'0']);
                // }
              ]
            ],

]?>

<?= GridView::widget([
    'id'=>'index',
    'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],

    'dataProvider'=>$dataProvider,
    'filterModel'=>$searchModel,
    'columns'=>$gridColumns,
    'resizableColumns'=>true,
    // 'containerOptions'=>['style'=>'overflow: auto'], // only set when $responsive = false
    'headerRowOptions'=>['class'=>'kartik-sheet-style'],
    'filterRowOptions'=>['class'=>'kartik-sheet-style'],
    'pjax'=>true, // pjax is set to always true for this demo
    // set your toolbar
    'toolbar'=> [
        '{export}',
        '{toggleData}',
    ],
    // parameters from the demo form
    'bordered'=>true,
    'striped'=>true,
    'condensed'=>true,
    'responsive'=>true,
    'hover'=>true,
    'showPageSummary'=>true,

    'panel'=>[
        'type'=>GridView::TYPE_SUCCESS,
        'heading'=>'Control de Ordenes',
    ],
    'persistResize'=>true,
    'toggleDataOptions'=>['minCount'=>10],
    'export'=>[
          // 'fontAwesome' => true,
          'fontAwesome'=>true,
          'PDF' => [
                  'options' => [
                      // 'title' => $tituloexport,
                      //  'subject' => $tituloexport,
                      // 'author' => 'NYCSPrep CIMS',
                      // 'keywords' => 'NYCSPrep, preceptors, pdf'
                  ]
              ],
          ],
    'exportConfig' => [
          GridView::EXCEL => [
            'label' => 'Guardar en XLS',
            'showHeader' => true,
            // 'filename' => $tituloexport,


          ],
          GridView::PDF => [
            'label' => 'Guardar en PDF',
            'showHeader' => true,
            'showCaption' => true,
            'showPageSummary' => true,
            'showFooter' => true,
             'title' => 'Reporte Piccolinas Pizza',
            'options' => ['title' =>'Reporte Piccolinas Pizza'],
            'config' => ['options' => ['title' =>'Reporte Piccolinas Pizza'],],
            'filename' =>'Reporte Piccolinas Pizza',
          ],
    ]
]);?>


<?= ActionBar::widget([
    'grid' => 'index',
    'templates' => [
        '{bulk-actions}' => ['class' => 'col-xs-4'],
    ],

]) ?>
</div>
