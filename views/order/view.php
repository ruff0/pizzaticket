<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\editable\Editable;
use yii\helpers\Url;
use kartik\mpdf\Pdf;
use yii\widgets\ActiveForm;
use yii2assets\printthis\PrintThis;
use yii\db\Expression;
use app\models\Order;

use app\models\OrderSearch;


$this->title = 'Orden ' . $model->id;

?>
<div class="order-view">

    <!-- <h3 class="pull-right"><?= Html::encode($this->title) ?></h3> -->
<br>

</br>
</br>
    <p>
      <?= Html::a('<b>Modificar</b>', ['update',
      'id' => $model->id,
      'phone' => $model->phone,
      'address' => $model->address,
      'neighborhood' => $model->neighborhood,
      'delivery' => $model->delivery,
      'size' => $model->size,
      'ingredients' => $model->ingredients,
      'quantity' => $model->quantity,
      'cheese_border' => $model->cheese_border,
      'money' => $model->money,
      'change' => $model->change,
      'subtotal' => $model->subtotal,
      'total' => $model->total,
      'wait' => $model->wait,], ['class' => ' pull-left btn btn-primary']) ?>
        &nbsp;        &nbsp;        &nbsp;        &nbsp;

        <?= Html::a('<b>Eliminar</b>', ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger pull-right fa fa-trash',
          'data' => [
            'confirm' => 'Seguro de eliminar este pedido?',
            'method' => 'post',
          ],
          ]) ?>

          &nbsp;        &nbsp;        &nbsp;        &nbsp;


          <?= Html::a('<b>Copiar datos</b>',[ 'order/create',
        'phone' => $model->phone,
        'address' => $model->address,
        'neighborhood' => $model->neighborhood,
        'delivery' => 0,],
        ['class' => 'btn btn-info'])?>
        &nbsp;        &nbsp;        &nbsp;        &nbsp;

        <?php $previousUrl = null;?>
        <?php $previousUrl = Url::previous();?>

        <!-- <?= Html::a('<i class="fa fa-file"></i> <b>PDF</b>', ['report', 'id'=>$model->id], [
          'class'=>'btn btn-warning',
          'target'=>'_blank',
          'data-toggle'=>'tooltip',
          'title'=>'El ticket aparecerá en una ventana nueva.'
        ]);
        ?> -->
        &nbsp;        &nbsp;        &nbsp;        &nbsp;
        <?= PrintThis::widget([
        	'htmlOptions' => [
        		'id' => 'PrintThis',
        		'btnClass' => 'btn btn-success',
        		'btnId' => 'btnPrintThis',
        		'btnText' => '<b>Imprimir</b>',
        		'btnIcon' => 'fa fa-print'
        	],
        	'options' => [
        		'debug' => false,
        		'importCSS' => false,
        		'importStyle' => false,
        		// 'loadCSS' => "path/to/my.css",
        		'pageTitle' => "Piccolinas Pizza",
        		'removeInline' => false,
        		'printDelay' => 333,
        		'header' => null,
        		'formValues' => true,
        	]
        ]);
        ?>
        &nbsp;        &nbsp;        &nbsp;        &nbsp;
        &nbsp;        &nbsp;        &nbsp;        &nbsp;



    </p>
  </br>
  </br>


  <div id="PrintThis">

    <?= DetailView::widget([
        'model' => $model,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],

        'attributes' => [
            [
              'attribute' => 'count',
              // 'value' => $count,
            ],
            // 'id',
            'phone',
            [
              'attribute' => 'wait',
                'format'=>['text', 2],

              ],
            'time',
            'only_date',
            'address',
            'neighborhood',
            ['attribute' => 'cheese_border',
            'value' => $model->cheese_border == '0' ? '' : 'Con Queso',
            'label' => $model->cheese_border == '0' ? '' : 'Orilla',
          ],
            'size',
            'quantity',
            'ingredients',
            'subtotal',
            'delivery',
            'total',
            'money',
            'change',
            // 'note',
            // 'status',
        ],
    ]) ?>

</div>
<meta http-equiv="refresh" content="25; URL=/index.php?r=order/indextoday">


</div>
