<?php
use kartik\time\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\DeliveryOrder;
use app\models\DeliveryOrderSearch;
use app\models\DeliveryBoy;
use app\models\DeliveryBoySearch;
use yii\helpers\ArrayHelper;


?>

<div class="delivery-order-form">


    <?php $request = Yii::$app->request;

    $model->id = $request->get('id_order');
    ?>

    <?php $form = ActiveForm::begin(); ?>
    <div class="row col-md-12">

      <div class="col-md-3">
        &nbsp;

<?= $form->field($model, 'id_delivery_name')->textInput()->label(false)->hiddenInput();
?>
</div>
    <?= $form->field($model, 'id_order')->textInput()->hiddenInput(['value' => $model->id])->label(false) ?>
<div class="col-md-2">
    <?= $form->field($model, 'delivery_time')->hiddenInput()->label(false);?>
  </div>
  <?= $form->field($model, 'delivery_time_back')->textInput()->label(false) ?>
  <div class="form-group">
    <?= Html::submitButton('Registrar Regreso', ['class' => 'btn btn-primary']) ?>
  </div>

</div>

    <?php ActiveForm::end(); ?>

</div>
