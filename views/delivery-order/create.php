<?php

use yii\helpers\Html;

$this->title = 'Create Delivery Order';

?>
<div class="delivery-order-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
