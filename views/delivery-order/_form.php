<?php
use kartik\time\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\DeliveryOrder;
use app\models\DeliveryOrderSearch;
use app\models\DeliveryBoy;
use app\models\DeliveryBoySearch;
use yii\helpers\ArrayHelper;
?>

<div class="delivery-order-form">
    <?php $request = Yii::$app->request;
    $model->id = $request->get('id_order');
    $rowvalue = $request->get('rowvalue');
    $amount = $request->get('amount');
    ?>

    <?php $form = ActiveForm::begin(); ?>
    <div class="row col-md-12">

      <div class="col-md-3">
        &nbsp;

<?= $form->field($model, 'id_delivery_name')->label(false)->widget(Select2::classname(), [
        'data' => ArrayHelper::map(DeliveryBoy::find()->all(), 'name', 'name'),
        // 'language' => 'de',
        'options' => ['placeholder' => 'Repartidor'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
?>
</div>
    <?= $form->field($model, 'id_order')->textInput(['value' => $model->id])->hiddenInput(['value' => $model->id])->label(false) ?>
    <?= $form->field($model, 'row_value')->textInput(['value' => $rowvalue])->hiddenInput(['value' => $rowvalue])->label(false) ?>
    <?= $form->field($model, 'amount')->textInput(['value' => $amount])->hiddenInput(['value' => $amount])->label(false) ?>

<div class="col-md-2">
    <?= $form->field($model, 'delivery_time')->label(false)->widget(TimePicker::classname(), [
      // 'value' => time(),
      'readonly' => false,
      'disabled' => false,

      'size' => 'md',
      'pluginOptions' => [
        'defaultTime' => 'current',
        // 'format' => 'H:mm',
        'minuteStep' => 1,
        'showMeridian' => false,
      ],

    ]);?>
  </div>
  <?= $form->field($model, 'delivery_time_back')->textInput()->hiddenInput()->label(false) ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',['name' => 'count', 'value' => $rowvalue, 'class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>

</div>

    <?php ActiveForm::end(); ?>

</div>
