<?php

use yii\helpers\Html;

?>
<div class="delivery-order-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_comeback_form', [
        'model' => $model,
    ]) ?>

</div>
