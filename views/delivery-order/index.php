<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use app\models\DeliveryBoy;
use yii\helpers\ArrayHelper;
use app\models\Order;
use mickgeek\actionbar\Widget as ActionBar;

?>
<div class="delivery-order-index">

  <?php $gridColumns =  [
      [
        'class' => 'kartik\grid\SerialColumn',
        'contentOptions'=>['class'=>'kartik-sheet-style'],
        'width'=>'36px',
        'header'=>'',
        'headerOptions'=>['class'=>'kartik-sheet-style']
      ],

      [
      'attribute'=>'id_delivery_name',
  ],


  ['attribute' => 'row_value',
  ],// 'id_order',
  'delivery_time',
  'delivery_time_back',
  'amount',
      [
        'class'=>'kartik\grid\CheckboxColumn',
      ],
      [
                'class' => 'kartik\grid\ActionColumn',
                'header'=>false,
                // 'pageSummary' => false,
                'options'=>['style'=>'width:150px;'],
                'buttonOptions'=>['class'=>'btn btn-default'],
                'template'=>'<div class="btn-group btn-group-sm text-center" role="group">

                {view}

                </div>',
                'buttons'=>[

                  // 'print'=>function($url,$model){
                  //   return Html::a('<i class="glyphicon glyphicon-print"></i>',['order/view'],['class'=>'btn-pdfprint btn btn-default','data-pjax'=>'0']);
                  // }
                ]
              ],

        // ['class' => 'kartik\grid\ActionColumn'],
  ]?>

<?php Pjax::begin(); ?>
<?= GridView::widget([
        'id'=>'delivery-order-index',
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns'=>$gridColumns,
        'resizableColumns'=>true,
        'headerRowOptions'=>['class'=>'kartik-sheet-style'],
        'filterRowOptions'=>['class'=>'kartik-sheet-style'],
        'pjax'=>true, // pjax is set to always true for this demo
        'toolbar'=> [
            '{export}',
          '{toggleData}',
        ],
        'bordered'=>true,
        'striped'=>true,
        'condensed'=>true,
        'responsive'=>true,
        'hover'=>true,
        'showPageSummary'=>true,
        'panel'=>[
            'type'=>GridView::TYPE_SUCCESS,
            'heading'=>'Control de Ordenes',
        ],
        'persistResize'=>true,
        'toggleDataOptions'=>['minCount'=>10],
        'export'=>[
              // 'fontAwesome' => true,
              'fontAwesome'=>true,
              'PDF' => [
                      'options' => [
                          // 'title' => $tituloexport,
                          //  'subject' => $tituloexport,
                          // 'author' => 'NYCSPrep CIMS',
                          // 'keywords' => 'NYCSPrep, preceptors, pdf'
                      ]
                  ],
              ],
        'exportConfig' => [
              GridView::EXCEL => [
                'label' => 'Guardar en XLS',
                'showHeader' => true,
                // 'filename' => $tituloexport,
              ],
              GridView::PDF => [
                'label' => 'Guardar en PDF',
                'showHeader' => true,
                'showCaption' => true,
                'showPageSummary' => true,
                'showFooter' => true,
                 'title' => 'Reporte Piccolinas Pizza',
                'options' => ['title' =>'Reporte Piccolinas Pizza'],
                'config' => ['options' => ['title' =>'Reporte Piccolinas Pizza'],],
                'filename' =>'Reporte Piccolinas Pizza',
              ],
        ]
    ]); ?>
<?php Pjax::end(); ?></div>

<?= ActionBar::widget([
    'grid' => 'delivery-order-index',
    'templates' => [
        '{bulk-actions}' => ['class' => 'col-xs-4'],
    ],

]) ?>
