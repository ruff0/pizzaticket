<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Order;
use app\models\OrderSearch;
use app\models\DeliveryBoy;
use app\models\DeliveryBoySearch;
use yii2assets\printthis\PrintThis;

use yii\helpers\ArrayHelper;

?>
<?php $request = Yii::$app->request;

$rowvalue = $request->get('rowvalue');
?>
<div class="delivery-order-view">

    <p>
      <div class="row col-md-12">
        <?= Html::a('Modificar', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        &nbsp;
        &nbsp;
<!--
        <?= Html::a('Registrar Regreso', ['comeback', 'id' => $model->id], ['class' => 'btn btn-success']) ?>

        &nbsp;
        &nbsp; -->
                    <?= PrintThis::widget([
                      'htmlOptions' => [
                        'id' => 'PrintThis',
                        'btnClass' => 'btn btn-warning',
                        'btnId' => 'btnPrintThis',
                        'btnText' => '<b>Imprimir</b>',
                        'btnIcon' => 'fa fa-print'
                      ],
                      'options' => [
                        'debug' => false,
                        'importCSS' => false,
                        'importStyle' => false,
                        // 'loadCSS' => "path/to/my.css",
                        'pageTitle' => "Piccolinas Pizza",
                        'removeInline' => false,
                        'printDelay' => 333,
                        'header' => null,
                        'formValues' => true,
                      ]
                    ]);
                    ?>

                  &nbsp;
                  &nbsp;

        <!-- <div class="pull-right">
        <?= Html::a('Eliminar', ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
            'confirm' => 'Are you sure you want to delete this item?',
            'method' => 'post',
          ],
          ]) ?>
        </div> -->

      </div>
      &nbsp;
      &nbsp;
    </p>
    <?php
    $dboy = DeliveryBoy::findOne(['id'=>$model->id_delivery_name]);

    $pay = Order::findOne(['id'=>$model->id_order]);
    ?>


    &nbsp;
    &nbsp;
    <div id="PrintThis">

    <?= DetailView::widget([
        'model' => $model,
        'formatter' => ['class' => 'yii\i18n\Formatter','nullDisplay' => ''],

        'attributes' => [
            // 'id',
            ['attribute' => 'id_delivery_name',
            'value' => $model->id_delivery_name,
            'label' => 'Repartidor',],
            ['attribute'=>'row_value',
            // 'value' => $rowvalue,
            'label' => 'Folio',
            ],
            ['attribute'=>'delivery_time',
            'label' => 'Salida'],


            ['attribute' => 'delivery_time_back',
            'value' => $model->delivery_time_back == null ? '' : $model->delivery_time_back,
            'label' => $model->delivery_time_back == null ? '' : 'Regreso',
          ],

          ['attribute'=>'amount',
           'value' => "$ ".$model->amount,
          'label' => 'Envio',
          ],
        ],
    ]) ?>

  </div>

        <?php
        echo "<table><tr><td class='col-md-3'>";
        echo "Pago del Cliente </td><td>$ "."$pay->money"."<br></tr>";
        echo "</td>";
        echo "<tr><td class='col-md-3'>";
        echo "Cambio al Cliente</td><td> $ "."$pay->change"."<br></tr>";
        echo "</td>";
        echo "<tr><td class='col-md-3'>";
        echo "Total de la Orden </td><td> $ "."$pay->total"."<br></tr>";
        echo "</td>";
        echo "<tr><td class='col-md-3'>";
        echo "Total sin Envio </td><td><b>$ "."$pay->subtotal"."</b><br></tr>";
        echo "</td>";
        echo "</tr></table>";

        ?>

</div>
