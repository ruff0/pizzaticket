<?php
namespace app\models;

use Yii;

class DeliveryBoy extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'delivery_boy';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'phone'], 'string', 'max' => 255],
            [['status', 'phone'], 'safe'],
            [['income'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Nombre',
            'status' => 'Status',
            'income' => 'Ingresos',
            'phone' => 'Teléfono',
        ];
    }
}
