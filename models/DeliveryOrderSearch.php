<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\DeliveryOrder;

/**
 * DeliveryOrderSearch represents the model behind the search form about `app\models\DeliveryOrder`.
 */
class DeliveryOrderSearch extends DeliveryOrder
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_order'], 'integer'],
            [['delivery_time', 'delivery_time_back',  'id_delivery_name',], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DeliveryOrder::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_delivery_name' => $this->id_delivery_name,
            'id_order' => $this->id_order,
            'delivery_time' => $this->delivery_time,
            'delivery_time_back' => $this->delivery_time_back,
        ]);

        return $dataProvider;
    }
}
