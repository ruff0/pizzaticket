<?php

namespace app\models;

use Yii;

class DeliveryOrder extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'delivery_order';
    }


    public function rules()
    {
        return [
            [[ 'id_order'], 'required'],
            [[ 'id_order', 'row_value', 'amount'], 'integer'],
            [['delivery_time', 'delivery_time_back', 'id_delivery_name'], 'safe'],
        ];
    }


    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_delivery_name' => 'Repartirador',
            'id_order' => 'Id Order',
            'delivery_time' => 'Hora de Salida',
            'delivery_time_back' => 'Hora de Retorno',
            'row_value' => 'Folio',
            'amount' => 'Envio',
        ];
    }
}
