<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id',  'subtotal', 'delivery', 'money', 'change', 'quantity'], 'integer'],
            [['cheese_border', 'size', 'phone', 'wait', 'time', 'date', 'only_date', 'address', 'neighborhood', 'ingredients', 'note', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
             'defaultOrder' => [
                 'id' => SORT_DESC,
             ]
          ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'wait' => $this->wait,
            'time' => $this->time,
            'date' => $this->date,
            // 'only_date' => $this->only_date,
            'cheese_border' => $this->cheese_border,
            'subtotal' => $this->subtotal,
            'delivery' => $this->delivery,
            'money' => $this->money,
            'change' => $this->change,
            'quantity' => $this->quantity,
            'status' => $this->status,
            'size' => $this->size,
        ]);

        $query->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'neighborhood', $this->neighborhood])
            ->andFilterWhere(['like', 'ingredients', $this->ingredients])
            ->andFilterWhere(['like', 'note', $this->note]);

            if(isset ($this->only_date)&&$this->only_date!=''){
              $date_explode=explode(" - ",$this->only_date);
              $date1=trim($date_explode[0]);
              $date2=trim($date_explode[1]);
              $query->andFilterWhere(['between','only_date',$date1,$date2]);
            }


        return $dataProvider;
    }
}
