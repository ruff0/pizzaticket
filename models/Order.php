<?php

namespace app\models;

use Yii;

class Order extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'orden';
    }

    public function rules()
    {
        return [
            [['wait', 'time', 'cheese_border', 'date', 'only_date', 'ingredients', 'change', 'status', 'size'], 'safe'],
            [['subtotal', 'count', 'total', 'delivery', 'money', 'quantity'], 'integer'],
            [['phone'], 'string', 'max' => 10],
            [['cheese_border', 'address', 'neighborhood', 'status'], 'string', 'max' => 60],
            [['note'], 'string', 'max' => 255],
            [['ingredients', 'total', 'size', 'subtotal', 'money'], 'required'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'Folio',
            'count' => 'Folio',
            'phone' => 'Teléfono',
            'wait' => 'Espera',
            'time' => 'Hora',
            'date' => 'Fecha',
            'address' => 'Dirección',
            'neighborhood' => 'Colonia',
            'cheese_border' => 'Orilla',
            'ingredients' => 'Ingredientes',
            'subtotal' => 'Costo',
            'delivery' => 'Envio',
            'money' => 'Paga con',
            'change' => 'Cambio',
            'quantity' => 'Cantidad',
            'note' => 'Anotaciones',
            'status' => 'Status',
            'total' => 'Total',
            'size' => 'Tamaño',
            'only_date' => 'Fecha',
        ];
    }
}
